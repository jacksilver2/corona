from matplotlib import pyplot as plt
import datetime as dt
import numpy as np
import requests
from smoothing_signal import smooth_out
# from corona.find_logistic import estimate_logistic
import scipy.optimize as opt


def get_sick_data():
    page = requests.get(
        'https://he.wikipedia.org/wiki/%D7%94%D7%AA%D7%A4%D7%A8%D7%A6%D7%95%D7%AA_%D7%A0%D7%92%D7%99%D7%A3_%D7%94%D7%A7%D7%95%D7%A8%D7%95%D7%A0%D7%94_%D7%91%D7%99%D7%A9%D7%A8%D7%90%D7%9C')
    search_term = 'display:inline-block">'
    l = list(page.text.split(search_term))

    data = []
    for i in range(1, l.__len__(), 2):
        data.append(int(l[i].split("<")[0].replace(",", "")))
    return data


def get_all_data(n=0):
    dates = []
    dead = []
    recovered = []
    sick = []
    page = requests.get(
        'https://he.wikipedia.org/w/index.php?title=%D7%94%D7%AA%D7%A4%D7%A8%D7%A6%D7%95%D7%AA_%D7%A0%D7%92%D7%99%D7%A3_%D7%94%D7%A7%D7%95%D7%A8%D7%95%D7%A0%D7%94_%D7%91%D7%99%D7%A9%D7%A8%D7%90%D7%9C&action=edit&section=5')
    data = page.text
    data = page.text.split('rows =')[1]
    data = data.split('| caption =')[0]
    data = data.replace('{{Medical cases chart/Row|', "")
    data = data.replace('}}', "").strip('\n')
    data = data.split('\n')
    for d in data[-n:]:
        pass
        sd = d.split('|')
        sd = [sd[i] if sd[i] != '' else '0' for i in range(sd.__len__())]
        dates.append(sd[0])
        dead.append(int(sd[1]))
        recovered.append(int(sd[2]))
        sick.append(int(sd[3]))

    return {'dates': dates, 'dead': dead, 'recovered': recovered, 'sick': sick}


def logistic_function(x, L, K, i0):
    return L / (1 + np.e ** (-K * (x - i0)))


def calc_implied_logistic(x_data, y_data, p0, projection_factor, func=logistic_function):
    (L_, K_, i0_), _ = opt.curve_fit(f=func, xdata=x_data, ydata=y_data, p0=p0)
    implied_logistic = logistic_function(np.arange(N * projection_factor), L_, K_, i0_)
    return implied_logistic, (L_, K_, i0_)


all_data = get_all_data()
num_sick = all_data['sick']
dates = all_data['dates']
dead = all_data['dead']
recovered = all_data['recovered']

N = num_sick.__len__()
tests = [
    np.inf,
    np.inf,
    np.inf,
    np.inf,
    104, 123, 134, 115, 158, 160, 173, 223, 126, 173, 300, 372, 473, 548, 482, 467, 660, 677, 573,
    1036, 1148, 1929, 2558, 2350, 2473, 1860, 3095, 3743, 5067, 5624, 5768, 5513, 5040, 6489,
    6636, 7581, 7833, 7294,
]

for _ in range(N - len(tests)):
    tests.append(tests[-1])

# print('sick', num_sick)
# print('dead', dead)
# print('recovered', recovered)



GROWTH_RATE = 1.265
PROJ_FACTOR = 8 // 4

# K_values = np.linspace(0, 0.3, 200)
# L_values = range(10000, 13000, 250)
# i0_values = range(30, 50, 1)

print('# data points:', N)

sick_deltas = [0] * N
sick_deltas[0] = num_sick[0]
for i in range(1, N):
    sick_deltas[i] = num_sick[i] - num_sick[i - 1]

dead_deltas = [0] * N
dead_deltas[0] = dead[0]
for i in range(1, N):
    dead_deltas[i] = dead[i] - dead[i - 1]

# apparent_exp = [GROWTH_RATE ** i for i in range(N)]
deltas_ratios = [0.0] * N
deltas_ratios[0] = 1.0
for i in range(1, N):
    if sick_deltas[i - 1] != 0:
        deltas_ratios[i] = sick_deltas[i] / sick_deltas[i - 1]
        continue
    else:
        deltas_ratios[i] = 1
deltas_ratios = np.asarray(deltas_ratios)

x = np.arange(N)

implied_sick_logistic, _ = calc_implied_logistic(
    x, np.array(num_sick), [11000, 0.22, 39], PROJ_FACTOR
)

sick_per_1000 = [(1000 * sick_deltas[i]) / (tests[i]) for i in range(N)]
normalized_num_sick = np.cumsum(sick_per_1000)
normalized_num_sick *= np.max(num_sick) / np.max(normalized_num_sick)

implied_norm_sick_logistic, (L, K, i0) = calc_implied_logistic(
    x, np.array(normalized_num_sick), [11000, 0.22, 39], PROJ_FACTOR
)

SICK_TO_DEATH = 20

bp_m_rate = np.zeros(N)
for i in range(SICK_TO_DEATH, N):
    d = dead[i]
    s = num_sick[i - SICK_TO_DEATH]
    # if s != 0:
    #     v = d / s
    # else:
    #     v = back_projected_mortality_rate[i - 1]
    bp_m_rate[i] = d / s
# print(bp_m_rate[np.nonzero(bp_m_rate)])
#
# best_mortality_rate = np.max(bp_m_rate[np.nonzero(bp_m_rate)])
# print('worst mortality rate:', best_mortality_rate)
# estimated_real_sick_num = np.array(dead) / best_mortality_rate
# plt.plot(estimated_real_sick_num)
# plt.show()
# exit()

dead_per_1000_sick = np.array([dead[i] / num_sick[i] for i in range(N)]) * 1000


def plot_all():
    # plt.plot(bp_m_rate)
    fig, axs = plt.subplots(2, 2)
    a, b, c, d = axs[0, 0], axs[0, 1], axs[1, 0], axs[1, 1]
    a.plot(num_sick, linewidth=2, color='blue')
    a.plot(implied_sick_logistic, "--", linewidth=0.8, color='blue')
    a.plot(normalized_num_sick, linewidth=2, color='green')
    a.plot(implied_norm_sick_logistic, "--", linewidth=0.8, color='green')
    a.text(num_sick.__len__(), num_sick[-1], num_sick[-1])
    a.text(60, 2000, "L:" + str(L))
    a.grid()
    a.set_title('corona')
    a.legend(
        ['#sick', 'logistic sick est.', 'norm. #sick', 'logistic norm. sick est.'])

    b.plot(deltas_ratios, color='red', linewidth=1)
    b.plot(smooth_out(deltas_ratios, W=2, s_type='average'), '--', color='crimson', linewidth=0.8)
    b.plot(np.ones(deltas_ratios.__len__()), '--', color='blue', linewidth=0.8)
    b.legend(['delta(n)/delta(n-1)', 'W=2 average'])

    # dl = np.asarray(deltas)
    # ts = np.asarray(tests)
    # axs[1, 0].plot(dl)
    # axs[1, 0].plot(ts)
    # axs[1, 0].legend(['daily sick deltas', 'daily tests'])
    # axs[1, 0].plot(dead)
    c.plot(dead_deltas)
    c.plot(dead_per_1000_sick)
    c.legend(['dead deltas', 'dead per 1,000 sick'])

    # if on day i we have a dead patient on record, this means that he was one
    # of the new sick patients on day i-20. suppose we have d new dead on day i and s new sick on
    # day i-20. we then say that the back-projected mortality rate, on day i is d/s

    d.legend(['dead deltas', 'dead per 1,000 sick'])

    d.plot(sick_per_1000, color='crimson', linewidth=1)
    d.text(sick_per_1000.__len__() - 1, sick_per_1000[-1], sick_per_1000[-1])
    d.plot(smooth_out(sick_per_1000, s_type='average'), '--', linewidth=0.7)
    d.legend(['new sick per 1,000 tests', 'W=5 average'])

    # plt.plot(sick_per_test,'red','--')
    # plt.plot(trunc_deltas,'green')
    # plt.plot(tests,'blue')
    # plt.bar(range(N), recovered, color='green')
    # for i in range(0,recovered.__len__(),4):
    #     plt.text(i-.2,recovered[i]+30,str(recovered[i]))
    # plt.bar(range(N), dead, color='red')
    # plt.gca().legend(['recovered', 'dead'])


plot_all()

plt.show()
