from pulp import *
import numpy as np
from matplotlib import pyplot as plt
from random_curve import random_natural_signal as rns, normalize


def logistic_function(L, K, i0, x):
    return L / (1 + np.e ** (-K * (x - i0)))


def generate_logistic(x_values, L, K, i0):
    K = min(K, 1)
    return np.asarray([
        logistic_function(
            L, K, i0, x
        ) for x in x_values
    ])


def logistics_MSE(signal, x_values, L, K, i0):
    candidate_signal = generate_logistic(x_values, L, K, i0)
    return np.mean(np.square(signal - candidate_signal))


def target_function(act_signal, x_values, K, L, i0):
    # given x_values over which
    # act_signal is given, calculates the
    # logistic function induced by K,L,i0
    # and returns the MSE between that function
    # and act_signal

    return logistics_MSE(act_signal,
                         x_values,
                         L, K, i0)


def main():
    N = 1000
    ACT_L = 20000
    ACT_i0 = 40
    ACT_K = 0.3
    NOISE_IMPACT = 0

    x_values = np.linspace(0, ACT_i0, N)
    act_signal = generate_logistic(x_values, ACT_L, ACT_K, ACT_i0)
    noise_signal = np.asarray(
        rns(n=N,
            around_zero=True,
            max_slope=100000000,
            min_stretch=3,
            max_stretch=20,
            smoothing_w=2
            )
    ) * np.max(act_signal) * NOISE_IMPACT
    clean_signal = np.copy(act_signal)
    act_signal += noise_signal

    # print('supposed MSE:', target_function(act_signal, x_values, 0.3, 15000, 40))
    # print('zero MSE?:', target_function(act_signal, x_values, ACT_K, ACT_L, ACT_i0))


    K_values = np.linspace(0, 1, 50)
    L_values = range(10000, 40000, 2000)
    i0_values = range(30, 80, 5)

    candidate_signal, ans = \
        estimate_logistic(L_values,
                          K_values,
                          i0_values,
                          act_signal,
                          x_values,
                          forecast_x_values=np.linspace(0, 2 * ACT_i0, 2 * N))
    print(ans)

    plt.plot(act_signal)
    plt.plot(candidate_signal, '--', linewidth=1)
    # print(np.mean(np.square(normalize(candidate_signal - act_signal))))
    plt.show()


def estimate_logistic(L_values, K_values, i0_values, act_signal, x_values, forecast_x_values, g_ans=None):
    if g_ans is None:
        K_n = K_values.__len__()
        L_n = L_values.__len__()
        i0_n = i0_values.__len__()
        min_mse = np.inf
        answer = {}
        for i in range(K_values.__len__()):
            for j in range(L_values.__len__()):
                for k in range(i0_values.__len__()):
                    cur_k = K_values[i]
                    cur_l = L_values[j]
                    cur_i0 = i0_values[k]
                    cur_mse = target_function(act_signal, x_values, cur_k, cur_l, cur_i0)
                    if cur_mse < min_mse:
                        min_mse = cur_mse
                        answer['L'] = cur_l
                        answer['K'] = cur_k
                        answer['i0'] = cur_i0
        answer['mse'] = min_mse
    else:
        print('I have the answer...')
        answer = g_ans
    candidate_signal = generate_logistic(forecast_x_values, answer['L'], answer['K'], answer['i0'])
    return candidate_signal, answer


if __name__ == '__main__':
    main()
